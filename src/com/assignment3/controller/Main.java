package com.assignment3.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	volatile static boolean running = true;
	volatile static boolean notEmpty = true;
	static int count = 0;

	public static void main(String[] args) throws IOException {
		final ArrayList<Integer> buffer = new ArrayList<Integer>();
		final Market market = new Market();

		Thread producerT = new Thread(new Runnable() {
			public void run() {
				try {

					while (running) {
						Thread.sleep(300);
						if (running) {
							count = market.producer(buffer, count);
						} else {
							System.out.print("Producer Terminated");
							break;
						}
					}
					System.out.print("Producer Terminated");
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					market.release(buffer);
				}
			}

		});

		Thread consumerT = new Thread(new Runnable() {
			public void run() {
				while (running || buffer.size() > 0 || notEmpty) {
					try {
						Thread.sleep(30);
						if (notEmpty) {
							market.consumer(buffer, count);
							if (buffer.isEmpty() && !running) {
								notEmpty = false;
								System.out.print("Consumer Terminated");
							}
						} else {
							System.out.print("Consumer Terminated");
							break;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		});
		producerT.start();
		consumerT.start();
		Scanner scanner = new Scanner(System.in);
		scanner.nextLine();
		running = false;
		notEmpty = true;
	}
}
class Market {
	synchronized int producer(ArrayList<Integer> buffer, int itemCount) {
		if (buffer.size() < 5) {
			itemCount++;
			buffer.add(itemCount);
			System.out.println("Produced Item :- " + itemCount);// Producer
																// adding items
																// in buffer
			notify();
			return itemCount;
		} else {
			try {
				wait(); // Holding the production till some room is produced
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return itemCount;
	}

	synchronized void consumer(ArrayList<Integer> buffer, int itemCount) {
		if (buffer.size() > 0) {
			System.out.println("Removed Item :- "
					+ buffer.get(buffer.size() - 1));// consumer consuming the
														// items in stack
														// fashion
			buffer.remove(buffer.size() - 1);
			notify();
		} else {
			try {
				if (itemCount > 0 && buffer.size() == 0) {
					wait(); // Waiting till items are available to consume
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	synchronized void release(ArrayList<Integer> buffer) {
		notify();
	}
}

